# WordCounter for Lemonade
by Idan Avisar

## Abstract
This project contains the code for the Word Counter application requested by Lemonade.

## How to Use
1. Enter the command `./gradlew bootRun` from the project root directory.
<br>This command will download gradle, download dependencies, build and run the
Groovy code.
2. The web application will run on `http://localhost:8080`
3. To add words, send a `POST` request to `/word/counter` with one
of the following parameters:
    1. `txt` - a string to be parsed for words
    2. `file` - full path to a local file on the machine running the application (the contents of the file will be used as input)
    3. `url` - the data returned from the URL will be used as input
4. To get a count of each word, send a `GET` request to `/word/statistics` with the following parameter:
    * `word` - the word to get a count for
    
## Persistence
By default, the system persists information in memory. That means that if you restart the application,
all previous data will be lost.

To ensure the data is persisted after the application is terminated, you must provide the full path to a file in which to store
persistence data.
Set this in the `dbFileName` property in *src/main/resources/application.properties*

## Limitations and Assumptions
* Local and Remote files should have line breaks every 1GB of data (otherwise application may run OOM)
* The application is case insensitive
* The application support *most* words in English (ascii characters)
* The application has partial support for words containing non alphanumeric characters. These include
    * Words containing an apostrophe (') (e.g. I'm, We'll, George's)
* The application will return a BAD_REQUEST when POST'ing to the counter endpoint with none of the required parameters
* The application will process only ONE of the parameters if more than one is supplied ( txt > file > url )

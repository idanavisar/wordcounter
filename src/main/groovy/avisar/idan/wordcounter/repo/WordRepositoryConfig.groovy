/*
 * Copyright 2017 Varada
 */

package avisar.idan.wordcounter.repo

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties
class WordRepositoryConfig {
    String dbFileName
}

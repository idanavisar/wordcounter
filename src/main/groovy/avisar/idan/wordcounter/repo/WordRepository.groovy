/*
 * Copyright 2017 Varada
 */

package avisar.idan.wordcounter.repo

import org.h2.mvstore.MVMap
import org.h2.mvstore.MVStore
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class WordRepository {
    private static final String WORDS = 'words'
    private final WordRepositoryConfig config
    private final MVStore mvStore

    @Autowired
    WordRepository(WordRepositoryConfig config) {
        this.config = config
        mvStore = MVStore.open(config.dbFileName)
    }

    /**
     * Returns a WordStorer via which the client can add words.
     * It is the client's responsibility to close the WordStorer
     * after adding all words to ensure the word stats are persisted.
     * @return
     */
    WordStorer newWordStorer() {
        return new MyWordStorer()
    }

    int getWordCount(String word) {
        MVMap<String, Integer> map = mvStore.openMap(WORDS)
        return map.get(word, 0)
    }

    interface WordStorer extends Closeable {
        void add(String word)
    }

    private class MyWordStorer implements WordStorer {
        final Map<String, Integer> words

        MyWordStorer() {
            words = mvStore.openMap(WORDS)
        }

        @Override
        void add(String word) {
            int n = words.getOrDefault(word, 0)
            words.put(word, n + 1)
        }

        @Override
        void close() throws IOException {
            mvStore.commit()
        }
    }
}
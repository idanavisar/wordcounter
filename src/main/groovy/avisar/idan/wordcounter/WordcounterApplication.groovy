package avisar.idan.wordcounter

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class WordcounterApplication {

	static void main(String[] args) {
		SpringApplication.run(WordcounterApplication, args)
	}

}

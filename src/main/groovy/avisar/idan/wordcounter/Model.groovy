/*
 * Copyright 2017 Varada
 */

package avisar.idan.wordcounter

import avisar.idan.wordcounter.repo.WordRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class Model {
    private static final String WORD_PATTERN = /\w+(?:'\w+)?/
    @Autowired
    WordRepository repo

    void countWordsInText(String text) {
        new ByteArrayInputStream(text.bytes).withReader {r -> storeWords(r) }
    }

    void countWordsInFile(String fileName) {
        new File(fileName).withReader { r -> storeWords(r) }
    }

    void countWordsInUrl(String url) {
        new URL(url).withReader {r-> storeWords(r) }
    }

    private void storeWords(Reader reader) {
        repo.newWordStorer().withCloseable {wordStorer ->
            reader.eachLine { line ->
                line.eachMatch(WORD_PATTERN) { word ->
                    wordStorer.add(word.toLowerCase())
                }
            }
        }
    }

    int getWordCount(String word) {
        repo.getWordCount(word.toLowerCase())
    }
}

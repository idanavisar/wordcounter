/*
 * Copyright 2017 Varada
 */

package avisar.idan.wordcounter

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping('word')
class Controller {
    @Autowired
    Model model

    @PostMapping('counter')
    void countWords(@RequestParam Map<String, String> allParams) {
        if (allParams.containsKey('txt')) {
            String text = allParams['txt']
            model.countWordsInText(text)
        } else if (allParams.containsKey('file')) {
            String fileName = allParams['file']
            model.countWordsInFile(fileName)
        } else if (allParams.containsKey('url')) {
            String url = allParams['url']
            model.countWordsInUrl(url)
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                              "Request should contain one of 'txt', 'file' or 'url' params")
        }

    }

    @GetMapping('statistics')
    int getWord(@RequestParam String word) {
        model.getWordCount(word)
    }
}
